import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs';

interface Options {
  headers?:
  | HttpHeaders
  | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?:
  | HttpParams
  | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  get(path, params?: { [param: string]: string | string[] }): Observable<any> {
    const token = localStorage.getItem('token');
    if (token) {
      Object.assign(params, { token });
    }
    return this.http.get(`${this.url}${path}`, {
      responseType: 'json',
      params
    });
  }

  post(
    path,
    body: any | null,
    options?: Options
  ): Observable<HttpResponse<any>> {
    // console.log(this.http.post(`${this.url}${path}`, body, options));
    const token = localStorage.getItem('token');

    const defaultOptions: Options = {
      observe: 'body',
      responseType: 'json'
    };
    // if (token) {
    //   options.headers['token'] = token;
    // }
    Object.assign(defaultOptions, options);
    return this.http.post<any>(`${this.url}${path}`, body, defaultOptions);
  }

  patch(path, body: any | null) {
    return this.http.patch(`${this.url}${path}`, body);
  }

  put(path, body: any | null) {
    return this.http.put(`${this.url}${path}`, body);
  }

  delete(path, body?: any): Observable<any> {
    return this.http.delete(`${this.url}${path}`, body);
  }

  download(
    path,
    params?: { [param: string]: string | string[] }
  ): Observable<any> {
    return this.http.get(`${this.url}${path}`, { responseType: 'blob' });
  }
}
