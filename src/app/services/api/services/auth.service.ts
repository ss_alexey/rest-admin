import { Injectable } from '@angular/core';

import { ApiService } from '../api.service';

import { UserRegister } from '../../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiAuthService {

  constructor(private apiService: ApiService) {
  }
  userRegister = (user) => {
    return this.apiService.post('/admin/auth/register', user);
  }
}
