import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularModule } from './angular/angular.module';
import { MaterialModule } from './material/material.module';

const modules = [CommonModule, AngularModule, MaterialModule];

@NgModule({
  declarations: [],
  imports: modules,
  exports: modules
})
export class SharedModule { }
