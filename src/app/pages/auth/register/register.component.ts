import { Component, OnInit } from '@angular/core';

import { ApiAuthService } from '../../../services/api/services';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { pipe } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formData = new FormData();
  imageSrc: ArrayBuffer | string;
  registerForm = new FormGroup({
    firstName: new FormControl('firstName', [
      Validators.required,
      Validators.minLength(5)
    ]),
    email: new FormControl('some@email.com', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('password', [
      Validators.required,
      Validators.minLength(5)
    ]),
    confirmPassword: new FormControl('password1', [
      Validators.required,
      Validators.minLength(5)
    ])
  });

  constructor(private apiAuthService: ApiAuthService) {}

  readURL(event: Event): void {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files[0]) {
      const file = (event.target as HTMLInputElement).files[0];
      const reader = new FileReader();
      this.formData.set('file', file);
      reader.onload = e => {
        this.imageSrc = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }
  onSubmit() {
    if (!this.registerForm.valid) {
      return;
    }
    const test = new FormData();
    const controls = this.registerForm.controls;
    for (const key in this.registerForm.controls) {
      if (controls.hasOwnProperty(key)) {
        test.set(key, controls[key].value);
      }
    }

    this.apiAuthService.userRegister(test).subscribe(
      res => console.log(res),
      (err: HttpErrorResponse) => {
        throw Error(err.message);
        console.log('ERROR', err);
      }
    );
  }
  ngOnInit() {
    this.registerForm.valueChanges.subscribe(res => {
      this.formData.set('body', JSON.stringify(res));
    });
  }
}
